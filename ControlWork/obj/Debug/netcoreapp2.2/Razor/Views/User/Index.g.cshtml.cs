#pragma checksum "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4586f8727a26591311f2d6a2018e30c87ee49725"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_User_Index), @"mvc.1.0.view", @"/Views/User/Index.cshtml")]
[assembly:global::Microsoft.AspNetCore.Mvc.Razor.Compilation.RazorViewAttribute(@"/Views/User/Index.cshtml", typeof(AspNetCore.Views_User_Index))]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#line 1 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\_ViewImports.cshtml"
using ControlWork;

#line default
#line hidden
#line 2 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\_ViewImports.cshtml"
using ControlWork.Models;

#line default
#line hidden
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4586f8727a26591311f2d6a2018e30c87ee49725", @"/Views/User/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"36b31422311ff2e9a8913111d0e6a4ae79b7ed41", @"/Views/_ViewImports.cshtml")]
    public class Views_User_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<IEnumerable<User>>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#line 2 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml"
  
    ViewData["Title"] = "Users";
    Layout = "~/Views/Shared/_Layout.cshtml";

#line default
#line hidden
            BeginContext(114, 9, true);
            WriteLiteral("<table>\r\n");
            EndContext();
#line 7 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml"
     foreach (var user in Model)
    {

#line default
#line hidden
            BeginContext(164, 30, true);
            WriteLiteral("        <tr>\r\n            <td>");
            EndContext();
            BeginContext(195, 9, false);
#line 10 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml"
           Write(user.Name);

#line default
#line hidden
            EndContext();
            BeginContext(204, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(228, 13, false);
#line 11 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml"
           Write(user.UserName);

#line default
#line hidden
            EndContext();
            BeginContext(241, 23, true);
            WriteLiteral("</td>\r\n            <td>");
            EndContext();
            BeginContext(265, 8, false);
#line 12 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml"
           Write(user.Age);

#line default
#line hidden
            EndContext();
            BeginContext(273, 22, true);
            WriteLiteral("</td>\r\n        </tr>\r\n");
            EndContext();
#line 14 "C:\Users\Somnium\Desktop\controlworkefsol\ControlWork\Views\User\Index.cshtml"
    }

#line default
#line hidden
            BeginContext(302, 14, true);
            WriteLiteral("</table>\r\n\r\n\r\n");
            EndContext();
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<IEnumerable<User>> Html { get; private set; }
    }
}
#pragma warning restore 1591
